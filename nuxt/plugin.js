import Vue from 'vue';
import VueComponents from '<%= options.ComponentBaseName %>.js';
import '<%= options.ComponentBaseName %>.css';
<% if (typeof (options.options) === "string") { %>
import VueComponentOptions from '<%= options.options %>';
<% } else { %>
const VueComponentOptions = <%= JSON.stringify(options.options) %>;
<% } %>

export default function() {
	Vue.use(VueComponents, VueComponentOptions);
}
