const path = require('path');

module.exports = function(moduleOptions) {
	moduleOptions = moduleOptions || {};
	const framework = moduleOptions.framework || 'buefy';

	this.addPlugin({
		src: path.resolve(__dirname, 'plugin.js'),
		options: {
			ComponentBaseName: getComponentBaseName(framework),
			options: moduleOptions.options || {},
		},
	});
};

// Get the component base name for the framework
function getComponentBaseName(framework) {
	if (framework === 'buefy') return path.resolve(__dirname, '../dist/node/vue-components-buefy');
	return path.resolve(__dirname, '../dist/node/vue-components');
}
