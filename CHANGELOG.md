# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.5.5](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.5.4...v0.5.5) (2019-09-10)


### Bug Fixes

* Setting new items data ([0226403](https://gitlab.com/renanhangai_/vue/vue-components/commit/0226403))

### [0.5.4](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.5.3...v0.5.4) (2019-09-06)


### Features

* New implementation of infinite-list ([5790145](https://gitlab.com/renanhangai_/vue/vue-components/commit/5790145))

### [0.5.3](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.5.2...v0.5.3) (2019-09-06)

## [0.5.2](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.5.1...v0.5.2) (2019-06-27)


### Bug Fixes

* Action in table was not being called ([51e317b](https://gitlab.com/renanhangai_/vue/vue-components/commit/51e317b))



## [0.5.1](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.5.0...v0.5.1) (2019-06-14)


### Bug Fixes

* Event listeners was not being passed to vc-table ([0f1ee3b](https://gitlab.com/renanhangai_/vue/vue-components/commit/0f1ee3b))



# [0.5.0](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.4.5...v0.5.0) (2019-06-13)


### Features

* Added table-search component ([9c7a146](https://gitlab.com/renanhangai_/vue/vue-components/commit/9c7a146))



## [0.4.5](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.4.4...v0.4.5) (2019-05-30)


### Bug Fixes

* Support for href ([046d8cf](https://gitlab.com/renanhangai_/vue/vue-components/commit/046d8cf))



## [0.4.4](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.4.3...v0.4.4) (2019-05-30)


### Features

* Added href support for vc-table ([ed9961d](https://gitlab.com/renanhangai_/vue/vue-components/commit/ed9961d))



## [0.4.3](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.4.2...v0.4.3) (2019-05-29)


### Bug Fixes

* Table was not working when actions was null ([2928b44](https://gitlab.com/renanhangai_/vue/vue-components/commit/2928b44))



## [0.4.2](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.4.1...v0.4.2) (2019-05-23)


### Bug Fixes

* Validation type of value in vc-input-file ([f2835a9](https://gitlab.com/renanhangai_/vue/vue-components/commit/f2835a9))



## [0.4.1](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.4.0...v0.4.1) (2019-05-23)


### Bug Fixes

* Input file status ([e3306e4](https://gitlab.com/renanhangai_/vue/vue-components/commit/e3306e4))



# [0.4.0](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.3.1...v0.4.0) (2019-05-23)


### Features

* New vc-input-mask ([b35ddf4](https://gitlab.com/renanhangai_/vue/vue-components/commit/b35ddf4))



## [0.3.1](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.3.0...v0.3.1) (2019-05-23)



# [0.3.0](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.2.0...v0.3.0) (2019-05-23)


### Bug Fixes

* Condition on nuxt module plugin ([e7af6a9](https://gitlab.com/renanhangai_/vue/vue-components/commit/e7af6a9))


### Features

* Improved options for nuxt module ([bbd0ae2](https://gitlab.com/renanhangai_/vue/vue-components/commit/bbd0ae2))



# [0.2.0](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.1.3...v0.2.0) (2019-05-23)


### Features

* Added vc-input-file component ([e94e8c2](https://gitlab.com/renanhangai_/vue/vue-components/commit/e94e8c2))



## [0.1.3](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.1.2...v0.1.3) (2019-05-16)



## [0.1.2](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.1.1...v0.1.2) (2019-05-16)


### Bug Fixes

* Declaration files ([008c1d0](https://gitlab.com/renanhangai_/vue/vue-components/commit/008c1d0))



## [0.1.1](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.1.0...v0.1.1) (2019-05-15)


### Bug Fixes

* Build of documentation ([720f030](https://gitlab.com/renanhangai_/vue/vue-components/commit/720f030))



# [0.1.0](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.9...v0.1.0) (2019-05-15)


### Features

* Added basic filters ([addf2e1](https://gitlab.com/renanhangai_/vue/vue-components/commit/addf2e1))



## [0.0.9](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.8...v0.0.9) (2019-05-15)


### Bug Fixes

* Action must not have a box shadow when disabled ([a497c44](https://gitlab.com/renanhangai_/vue/vue-components/commit/a497c44))



## [0.0.8](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.7...v0.0.8) (2019-05-15)


### Bug Fixes

* Do not call to or anchor when disabled ([a438be9](https://gitlab.com/renanhangai_/vue/vue-components/commit/a438be9))



## [0.0.7](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.6...v0.0.7) (2019-05-15)


### Bug Fixes

* Added anchor properties on action ([cb88441](https://gitlab.com/renanhangai_/vue/vue-components/commit/cb88441))



## [0.0.6](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.5...v0.0.6) (2019-05-15)



## [0.0.5](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.4...v0.0.5) (2019-05-11)


### Bug Fixes

* InfiniteList now uses typescript ([ea2ed75](https://gitlab.com/renanhangai_/vue/vue-components/commit/ea2ed75))



## [0.0.4](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.3...v0.0.4) (2019-05-10)


### Bug Fixes

* Table styles ([c4adb63](https://gitlab.com/renanhangai_/vue/vue-components/commit/c4adb63))



## [0.0.3](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.2...v0.0.3) (2019-05-10)


### Bug Fixes

* Missing path on nuxt module ([eb52069](https://gitlab.com/renanhangai_/vue/vue-components/commit/eb52069))



## [0.0.2](https://gitlab.com/renanhangai_/vue/vue-components/compare/v0.0.1...v0.0.2) (2019-05-10)



## 0.0.1 (2019-05-10)
