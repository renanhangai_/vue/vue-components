# vue-components

Vue sample components

# Nuxt

On `nuxt.config.js`

```js
module.exports = {
	// ...
	modules: [
		// ...
		[
			'@renanhangai/vue-components/nuxt',
			{
				framework: 'buefy',
				options: {
					// Framework options
				},
			},
		],
	],
};
```

BuefyOptions

```ts
export type VueComponentBuefyPluginOptions = {
	prefix?: string;

	inputFileIconDefault?: string;
	inputFileIconDisabled?: string;
	inputFileIconRemove?: string;

	// More masks
	inputMasks?: {
		[key: string]: any;
	};
};
```

# Components

<!-- START COMPONENTS -->

# InfiniteList

Creates an infinite list. The list will grow as every item is being filled.

## Props

<!-- @vuese:InfiniteList:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|item|The item to be generate when the list requires a new one|`Function` / `Object`|`false`|-|
|isItemEmpty|Check if the item is empty (to be removed from the list)|`Function`|`false`|isItemEmptyDefault|
|itemClass|The item class|`String`|`false`|-|
|transition|The transition name to apply|`String`|`false`|'transition'|
|data|Data to pass to the item slot|`Object`|`false`|-|
|value|Array of items|`Array`|`false`|-|
|disabled|Check if can create new items|`Boolean`|`false`|-|

<!-- @vuese:InfiniteList:props:end -->


## Events

<!-- @vuese:InfiniteList:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|

<!-- @vuese:InfiniteList:events:end -->




# Table

Better table for buefy

## Props

<!-- @vuese:Table:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|items|-|`Array`|`false`|-|
|columns|-|`Array`|`false`|-|
|actions|-|`Array`|`false`|-|
|rowTo|-|`Function`|`false`|-|
|loading|-|`Boolean`|`false`|-|
|emptyText|-|`String`|`false`|'Nenhum item encontrado'|

<!-- @vuese:Table:props:end -->


## Events

<!-- @vuese:Table:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|click|-|-|

<!-- @vuese:Table:events:end -->




# InputFile

## Props

<!-- @vuese:InputFile:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|value|-|`String` / `File`|`false`|-|
|disabled|-|`Boolean`|`false`|-|
|loading|-|`Boolean`|`false`|-|
|removable|-|`Boolean`|`false`|true|
|placeholder|-|`String`|`false`|null|

<!-- @vuese:InputFile:props:end -->


## Events

<!-- @vuese:InputFile:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|input|-|-|

<!-- @vuese:InputFile:events:end -->




# TableSearch

## Props

<!-- @vuese:TableSearch:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|search|-|`Array` / `String`|`true`|-|
|searchOptions|-|`Object`|`false`|-|
|searchPlaceholder|-|`String`|`false`|'Digite para searchr'|
|items|-|`Array`|`false`|-|

<!-- @vuese:TableSearch:props:end -->






<!-- END COMPONENTS -->
