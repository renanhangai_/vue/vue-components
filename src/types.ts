import Vue from 'vue';

export type VueComponentPluginOptions = {
	prefix?: string;
	filters?:
		| false
		| {
				[key: string]: (v: string, ...args: any[]) => any;
		  };
	libraries?: {
		Fuse?: any;
	};
};

export interface VueComponentFilter {
	// Address filter
	/**
	 * Apply a cnpj filter
	 * @param v Value to be filtered
	 */
	cep(value: string | number): string;
	/**
	 * Apply a cnpj filter
	 * @param v Value to be filtered
	 */
	cpf(value: string | number): string;
	/**
	 * Apply a cnpj filter
	 * @param v Value to be filtered
	 */
	cpfcnpj(value: string | number): string;
	// Utils filter
	/**
	 * Apply a cnpj filter
	 * @param v Value to be filtered
	 */
	get<T, Key extends keyof T, U>(value: T, path: Key): T[Key];
	get<T, Key extends keyof T, U>(value: T, path: Key, defaultValue: U): U | T[Key];
	/**
	 * Returns the value
	 * @param v Value to be filtered
	 */
	placeholder<T>(value: T, placeholder?: string): T | string;

	/// Other properties
	[key: string]: (v: any, ...args: any[]) => any;
}

// Augmentation for vue using filter
declare module 'vue/types/vue' {
	interface Vue {
		/**
		 * List of filters used by vue-components
		 */
		$f: VueComponentFilter;
	}
}
