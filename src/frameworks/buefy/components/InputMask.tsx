import Vue from 'vue';
import Component from 'vue-class-component';
import { mask } from 'vue-the-mask';
import { VMoney } from 'v-money';
import getValue from 'get-value';

/**
 * Default masks
 */
const MASKS = {
	cep: '#####-###',
	cpf: '###.###.###-##',
	cnpj: '##.###.###/####-##',
	cpfcnpj: ['###.###.###-##', '##.###.###/####-##'],
	telefone: ['(##) ####-####', '(##) #####-####'],
	date: '##/##/####',
	decimal: {
		money: true,
	},
};

/// Componente de input
@Component<VcInput>({
	inheritAttrs: false,
	directives: {
		mask: lazyDirective(mask),
		money: lazyDirective(VMoney),
	},
	props: {
		value: String,
		mask: String,
		placeholder: String,
	},
	watch: {},
})
export default class VcInput extends Vue {
	value!: string;
	mask!: string;
	placeholder!: string;

	get maskOptions() {
		if (!this.mask) return null;
		// Get the mask
		let options = getValue(this, ['$vueComponentsOptions', 'inputMasks', this.mask]) || MASKS[this.mask];
		if (!options) {
			console.error(`Invalid mask ${this.mask}`);
			return null;
		}
		// Call the function using the value
		if (typeof options === 'function') {
			options = options.call(null, { value: this.value, mask: this.mask });
		}

		// When money, defaults to , for decimal and . for thousands
		if (options.money) {
			return {
				decimal: ',',
				thousands: '.',
				precision: 2,
				...options,
			};
		}
		return options;
	}
	/// On input
	onInput(v) {
		this.$nextTick(() => {
			this.$emit('input', v);
		});
	}
	/// Get input props
	get inputProps() {
		return {
			attrs: this.$attrs,
			on: {
				input: this.onInput,
			},
			...this.maskProps,
		};
	}
	/// Get mask directive
	get maskProps() {
		if (!this.maskOptions) return null;
		if (this.maskOptions.money) {
			return {
				directives: [
					{
						name: 'money',
						value: this.maskOptions,
					},
				],
			};
		}
		return {
			directives: [
				{
					name: 'mask',
					value: this.maskOptions,
				},
			],
		};
	}
	/// Render the element
	render(h) {
		return <b-input value={this.value} placeholder={this.placeholder} {...this.inputProps} />;
	}
}

// Create a lazy directive
function lazyDirective(directive) {
	return function(element, binding) {
		Vue.nextTick(function() {
			directive(element, binding);
		});
	};
}
