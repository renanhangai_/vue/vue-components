/// Table row link
export default {
	functional: true,
	props: {
		to: null,
	},
	render(h, { props }) {
		if (!props.to) return null;
		if (props.to.href) {
			return (
				<a
					class="vc-table__row-link"
					href={props.to.href}
					target={props.to.target}
					download={props.to.download}
				/>
			);
		}
		return <router-link class="vc-table__row-link" to={props.to} />;
	},
};
