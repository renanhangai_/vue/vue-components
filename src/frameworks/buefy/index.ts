import { VueConstructor } from 'vue';
import { VueComponentBuefyPluginOptions } from './types';
import BuefyInputFile from '~/frameworks/buefy/components/InputFile.vue';
import BuefyInputMask from '~/frameworks/buefy/components/InputMask.tsx';
import BuefyTable from '~/frameworks/buefy/components/Table.vue';
import VueComponents from '~/index';

export default {
	install(vue: VueConstructor, options?: VueComponentBuefyPluginOptions) {
		options = {
			prefix: 'vc',
			...options,
		};
		vue.use(VueComponents, options);
		vue.component(`${options.prefix}-input-file`, BuefyInputFile);
		vue.component(`${options.prefix}-input-mask`, BuefyInputMask);
		vue.component(`${options.prefix}-table`, BuefyTable);
	},
};
