/**
 * Plugin options
 */
export type VueComponentBuefyPluginOptions = {
	prefix?: string;

	inputFileIconDefault?: string;
	inputFileIconDisabled?: string;
	inputFileIconRemove?: string;

	// More masks
	inputMasks?: {
		[key: string]: any;
	};
};

// Table
// Column filter options
export type VueComponentBuefyTableColumnFilter = string | ((v: any) => string);
// Column options
export type VueComponentBuefyTableColumn = {
	field: string;
	label: string;
	width?: string;
	numeric?: boolean;
	centered?: boolean;
	filter?: VueComponentBuefyTableColumnFilter | VueComponentBuefyTableColumnFilter[];
};
// Callback or value type
export type VueComponentBuefyTableCallableOrValue<T> = (item: any) => T | T;
// Table Value Type
export type VueComponentBuefyTableValue<T> = T extends VueComponentBuefyTableCallableOrValue<infer U> ? U : T;

/**
 * Table action
 */
export type VueComponentBuefyTableAction = {
	// Icon to show
	icon: VueComponentBuefyTableCallableOrValue<string>;
	// Icon color
	color?: VueComponentBuefyTableCallableOrValue<string>;
	// Action is disabled
	disabled?: VueComponentBuefyTableCallableOrValue<boolean>;
	// Event when click
	click?: (item: any) => any;
	to?: VueComponentBuefyTableCallableOrValue<string | { path: string }>;
	anchor?: VueComponentBuefyTableCallableOrValue<any>;
};
export type VueComponentBuefyTableActionNormalized = {
	[K in keyof VueComponentBuefyTableAction]: VueComponentBuefyTableValue<VueComponentBuefyTableAction[K]>
};
