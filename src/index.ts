import { VueConstructor } from 'vue';
import { VueComponentPluginOptions, VueComponentFilter } from './types';
import Filters from './filters';
import InfiniteList from './components/InfiniteList.vue';

export * from './types';
export * from './frameworks/buefy/types';

export default {
	install(vue: VueConstructor, options: VueComponentPluginOptions) {
		options = {
			prefix: 'vc',
			...options,
		};
		vue.use(Filters, options);
		vue.component(`${options.prefix}-infinite-list`, InfiniteList);

		// Libraries
		options.libraries = { ...options.libraries };
		normalizeLibrary(options, 'Fuse', () => require('fuse.js'));

		// Vue component options
		Object.defineProperty(vue.prototype, '$vueComponentsOptions', {
			enumerable: false,
			configurable: true,
			value: options,
		});
	},
};

function normalizeLibrary(options: any, key: string, cb: () => any) {
	if (options.libraries[key] === false) return;
	else if (options.libraries[key] === true || options.libraries[key] == null)
		options.libraries[key] = tryRequire(key, cb);
}

function tryRequire(lib: string, cb: () => any) {
	try {
		let m = cb();
		if ('default' in m) m = m.default;
		return m;
	} catch (err) {
		console.log(`${lib} not found`);
		return null;
	}
}
