export const Filters = {
	cnpj(value: string | number): string {
		if (!value) return '';
		value = `${value}`.replace(/[^\d+]/g, '');
		if (!value) return '';
		return `${value.substr(0, 2)}.${value.substr(2, 3)}.${value.substr(5, 3)}/${value.substr(8, 4)}-${value.substr(
			12,
			2
		)}`;
	},
	/// Filter by cpf
	cpf(value: string | number): string {
		if (!value) return '';
		value = `${value}`.replace(/[^\d+]/g, '');
		if (!value) return '';
		return `${value.substr(0, 3)}.${value.substr(3, 3)}.${value.substr(6, 3)}-${value.substr(9, 2)}`;
	},
	/**
	 * Filter by cpfcnpj
	 * @param value The value to filter
	 */
	cpfcnpj(value: string | number): string {
		if (!value) return '';
		value = `${value}`.replace(/[^\d+]/g, '');
		return value.length <= 11 ? Filters.cpf(value) : Filters.cnpj(value);
	},
};
