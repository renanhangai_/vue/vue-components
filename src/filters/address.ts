export const Filters = {
	cep(value: string | number) {
		if (!value) return '';
		value = `${value}`.replace(/[^\d+]/g, '');
		if (!value) return '';
		return `${value.substr(0, 5)}-${value.substr(5, 3)}`;
	},
};
