import { VueConstructor } from 'vue';
import { VueComponentPluginOptions } from '../types';

import { Filters as AddressFilters } from './address';
import { Filters as DocumentFilters } from './document';
import { Filters as UtilsFilters } from './utils';

const FilterList = [AddressFilters, DocumentFilters, UtilsFilters];

export default {
	install(vue: VueConstructor, options: VueComponentPluginOptions) {
		options = {
			prefix: 'vc',
			...options,
		};
		if (options.filters === false) return;
		let filters: any = {};
		FilterList.forEach((currentFilter: any) => {
			if (typeof currentFilter === 'function') currentFilter = currentFilter.call(null, options);
			filters = { ...filters, ...currentFilter };
		});
		filters = { ...filters, ...options.filters };
		Object.defineProperty(vue.prototype, '$f', {
			enumerable: false,
			configurable: true,
			value: filters,
		});
		for (const key in filters) {
			vue.filter(key, filters[key]);
		}
	},
};
