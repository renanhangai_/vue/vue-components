import { pathOr } from 'ramda';

export const Filters = {
	get<T, Key extends keyof T, U>(value: T, path: Key, defaultValue: U | null = null): U | T[Key] | null {
		return pathOr(defaultValue, path, value);
	},
	placeholder<T>(value: T, placeholder: string = '---'): T | string {
		return value ? value : placeholder;
	},
};
