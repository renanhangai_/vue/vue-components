const path = require('path');
const globby = require('globby');
const fs = require('fs-extra');

function replaceSection(text, sectionStart, sectionEnd, replacedContent) {
	const sectionStartIndex = text.indexOf(sectionStart);
	const sectionEndIndex = text.lastIndexOf(sectionEnd);
	if (sectionStartIndex < 0 || sectionEndIndex < 0) throw new Error(`Section not found`);

	const start = text.substr(0, sectionStartIndex + sectionStart.length) + '\n';
	const end = '\n' + text.substr(sectionEndIndex);
	return [start, replacedContent, end].join('\n');
}

async function main() {
	const readmeFilePath = path.resolve(__dirname, '../../README.md');
	const inputContent = await fs.readFile(readmeFilePath, 'utf8');
	const files = await globby('dist/docs/**/*.md');
	const outputBuffers = [];
	for (const file of files) {
		const buffer = await fs.readFile(file);
		outputBuffers.push(buffer, Buffer.from('\n\n'));
	}

	const outputBuffer = Buffer.concat(outputBuffers);

	const outputContent = replaceSection(
		inputContent,
		'<!-- START COMPONENTS -->',
		'<!-- END COMPONENTS -->',
		outputBuffer.toString('utf8')
	);
	await fs.writeFile(readmeFilePath, outputContent, 'utf8');
}

main();
