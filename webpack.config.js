const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

function getOptionsFromMode(mode) {
	if (mode === 'node') {
		return { isDev: true, isNode: true };
	} else if (mode === 'production') {
		return { isDev: false, isNode: false };
	}
	return { isDev: true, isNode: false };
}

function generateConfig({ mode }) {
	const { isDev, isNode } = getOptionsFromMode(mode);
	const targets = ['> 1%', 'ie >= 11'];
	const babelLoader = {
		loader: 'babel-loader',
		options: {
			presets: ['@vue/babel-preset-jsx', ['@babel/preset-env', { targets: targets }]],
			plugins: ['babel-plugin-ramda'],
		},
	};
	const externals = isNode && {
		'vue-class-component': 'vue-class-component',
		'get-value': 'get-value',
	};
	return {
		mode: isDev ? 'development' : 'production',
		target: isNode ? 'node' : 'web',
		devtool: isDev ? 'inline-source-map' : false,
		entry: {
			'vue-components': './src/index.ts',
			'vue-components-buefy': './src/frameworks/buefy/index.ts',
		},
		output: {
			path: path.resolve(__dirname, isNode ? 'dist/node' : 'dist'),
			filename: isDev ? '[name].js' : '[name].min.js',
			library: 'VueComponents',
			libraryTarget: isNode ? 'commonjs2' : 'umd',
		},
		resolve: {
			extensions: ['.tsx', '.ts', '.jsx', '.js', '.json'],
			alias: {
				'~': path.resolve(__dirname, 'src'),
			},
		},
		module: {
			rules: [
				{
					test: /\.m?js$/,
					exclude: /(node_modules|bower_components)/,
					use: babelLoader,
				},
				{
					test: /\.vue$/,
					loader: 'vue-loader',
					options: {
						optimizeSSR: false,
					},
				},
				{
					test: /\.tsx?$/,
					exclude: /node_modules/,
					use: [
						babelLoader,
						{
							loader: 'ts-loader',
							options: {
								appendTsSuffixTo: [/\.vue$/],
							},
						},
					],
				},
				{
					test: /\.pug?$/,
					use: 'pug-plain-loader',
					exclude: /node_modules/,
				},
				{
					test: /\.scss$/,
					use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
				},
			],
		},
		plugins: [
			new VueLoaderPlugin(),
			new MiniCssExtractPlugin({
				filename: isDev ? '[name].css' : '[name].min.css',
			}),
		],
		externals: {
			vue: {
				commonjs: 'vue',
				commonjs2: 'vue',
				amd: 'vue',
				root: 'Vue',
				var: 'Vue',
			},
			'fuse.js': {
				commonjs: 'fuse.js',
				commonjs2: 'fuse.js',
				amd: 'fuse.js',
				root: 'Fuse',
				var: 'Fuse',
			},
			...externals,
		},
	};
}

module.exports = generateConfig({
	mode: process.env.BUILD_MODE,
});
