const path = require('path');

module.exports = {
	genType: 'markdown',
	include: 'src/**/*.vue',
	markdownDir: path.resolve(__dirname, 'dist/docs'),
};
